package apache_chain;

import org.apache.commons.chain.Context;
import org.apache.commons.chain.Filter;

/**
 * Created by kevingamboa17 on 10/6/17.
 */
public class AuditFilter implements Filter {
    int totalAmountToBeWithdrawn;
    int noOfHundredsDispensed;
    int noOfFiftiesDispensed;
    int noOfTensDispensed;
    int amountLeftToBeWithdrawn;


    @Override
    public boolean postprocess(Context context, Exception exception) {
        // send notification to bank and user
        totalAmountToBeWithdrawn = (int)context.get("totalAmountToBeWithdrawn");
        noOfHundredsDispensed = (int) context.get("noOfHundredsDispensed");
        noOfFiftiesDispensed = (int) context.get("noOfFiftiesDispensed");
        noOfTensDispensed = (int) context.get("noOfTensDispensed");
        amountLeftToBeWithdrawn = (int) context.get("amountLeftToBeWithdrawn");

        System.out.println("Total amount: " + String.valueOf(totalAmountToBeWithdrawn));
        System.out.println("hundred amount: " + String.valueOf(noOfHundredsDispensed));
        System.out.println("fifties amount: " + String.valueOf(noOfFiftiesDispensed));
        System.out.println("ten amount: " + String.valueOf(noOfTensDispensed));
        System.out.println("left amount: " + String.valueOf(amountLeftToBeWithdrawn));
        return false;
    }

    @Override
    public boolean execute(Context context) throws Exception {
        return false;
    }
}

