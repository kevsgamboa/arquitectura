package apache_chain.Commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

/**
 * Created by kevingamboa17 on 10/6/17.
 */
public class FiftyDenominationDispenser implements Command {

    @Override
    public boolean execute(Context context) throws Exception {
        int amountLeftToBeWithdrawn = (int) context.get("amountLeftToBeWithdrawn");
        if (amountLeftToBeWithdrawn >= 50) {
            context.put("noOfFiftiesDispensed", amountLeftToBeWithdrawn / 50);
            context.put("amountLeftToBeWithdrawn", amountLeftToBeWithdrawn % 50);
        }
        return false;
    }
}
