package kwic_chain;

import org.apache.commons.chain.Context;
import org.apache.commons.chain.Filter;

import java.util.ArrayList;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class KwicPrinterFilter implements Filter {
    @Override
    public boolean postprocess(Context context, Exception e) {
        ArrayList<String[]> phrasesCombinations = (ArrayList<String[]>)context.get("phrasesCombinations");
        for (String[] combinations: phrasesCombinations) {
            for (int i=0; i< combinations.length; i++) {
                System.out.println(combinations[i]);
            }
            System.out.println("=================================");
        }

        return false;
    }

    @Override
    public boolean execute(Context context) throws Exception {
        return false;
    }
}
