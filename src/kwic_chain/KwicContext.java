package kwic_chain;

import org.apache.commons.chain.impl.ContextBase;

import java.util.ArrayList;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class KwicContext extends ContextBase {
    int numberOfPhrases;
    String userInput;
    String[] originalPhrases;
    ArrayList<String[]> phrasesCombinations;
}
