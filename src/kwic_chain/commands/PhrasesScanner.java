package kwic_chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import java.util.Scanner;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class PhrasesScanner implements Command {


    private String extractPhrase(){
        System.out.println("Introduce tu frase:");
        return new Scanner(System.in).nextLine();
    }


    @Override
    public boolean execute(Context context) throws Exception {
        int numberOfPhrasesToScan = (int)context.get("numberOfPhrases");
        String[] phrases = new String[numberOfPhrasesToScan];
        for (int i=0; i < numberOfPhrasesToScan; i++) {
            phrases[i] = extractPhrase();
        }

        context.put("originalPhrases", phrases);

        return false;
    }
}
