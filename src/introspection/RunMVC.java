package introspection;//RunMVC.java
//(C) Joseph Mack 2011, jmack (at) wm7d (dot) net, released under GPL v3 (or any later version)

public class RunMVC {

    //The order of instantiating the objects below will be important for some pairs of commands.
    //I haven't explored this in any detail, beyond that the order below works.

    private int start_value = 10;	//initialise model, which in turn initialises view

    public RunMVC(int i) {

        //create Model and View
        Model myModel 	= new Model();
        View myView 	= new View();

        //tell Model about View.
        myModel.addObserver(myView);

			/*init model after view is instantiated and can show the status of the model
			(I later decided that only the controller should talk to the model
			and moved initialisation to the controller (see below).)*/

        //uncomment to directly initialise Model
        //myModel.setValue(start_value);

        //create Controller. tell it about Model and View, initialise model
        Controller myController = new Controller();
        myController.addModel(myModel);
        myController.addView(myView);
        myController.initModel(start_value);

        //tell View about Controller
        myView.addController(myController);
        //and Model,
        //this was only needed when the view inits the model
        //myView.addModel(myModel);

    } //RunMVC()

    public RunMVC() {
        Class[] classes = getClasses("");
        Object[] objects = new Object[classes.length];

        try {
            // Generate one instance of every class
            for (int i=0; i<classes.length-2; i++) {
                objects[i] = classes[i].newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Hardcoded

        try {
            // Execute the myModel.addObserver(myView)
            classes[0].getDeclaredMethod("addObserver", classes[1])
            .invoke(objects[0], objects[1]);

            // Execute the myController.addModel(myModel);
            classes[2].getDeclaredMethod("addModel", classes[0])
            .invoke(objects[2], objects[0]);

            // Execute the myController.addView(myView);
            classes[2].getDeclaredMethod("addView", classes[1])
            .invoke(objects[2], objects[1]);

            // Execute the myController.initModel(start_value);
            classes[2].getDeclaredMethod("initModel", Integer.TYPE)
            .invoke(objects[2], start_value);

            // Execute the myView.addController(myController);
            classes[1].getDeclaredMethod("addController", classes[2])
            .invoke(objects[1], objects[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private  Class[] getClasses(String content) {
        // Super hardcoded
        try {
            Class[] classes = new Class[5];
            classes[0] = Class.forName("Model");
            classes[1] = Class.forName("View");
            classes[2] = Class.forName("Controller");
            return classes;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }






} //RunMVC